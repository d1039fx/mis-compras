import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:to_buy_app/login/router/login_router.dart';
import 'package:to_buy_app/principal/bloc/responsive_bloc.dart';
import 'package:to_buy_app/principal/router/principal_router.dart';

class ToBuyApp extends StatelessWidget with LoginRouter, PrincipalRouter {
  const ToBuyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ResponsiveBloc>(create: (context) => ResponsiveBloc())
      ],
      child: MaterialApp.router(
        title: 'To buy app',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        routerConfig: GoRouter(
            initialLocation: '/splash', routes: <GoRoute>[...pathLoginList(), ...pathPrincipalList()]),
      ),
    );
  }
}

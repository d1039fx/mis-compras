import 'package:flutter/gestures.dart';

abstract class PositionalLateralInterfaces{
  ({double x, double y}) onDrag({required DragUpdateDetails details});
  ({double width, double height}) onResize({required DragUpdateDetails details});
}
typedef PositionalLateralMenu = ({
  double width,
  double height,
  double verticalPos,
  double horizontalPos
});

PositionalLateralMenu positionalLateralMenuFromMap(Map<String, dynamic> json) => (
      width: json['width'],
      height: json['height'],
      horizontalPos: json['horizontalPos'],
      verticalPos: json['verticalPos']
    );

Map<String, dynamic> positionalLateralMenuToMap({required PositionalLateralMenu pos}) => {
      'width': pos.width,
      'height': pos.height,
      'horizontalPos': pos.horizontalPos,
      'verticalPos': pos.verticalPos
    };

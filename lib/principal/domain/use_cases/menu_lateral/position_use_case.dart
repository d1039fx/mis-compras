import 'package:flutter/gestures.dart';
import 'package:to_buy_app/principal/data/manager/menu_lateral_position.dart';
import 'package:to_buy_app/principal/domain/entities/lateral_menu/positional_lateral_menu.dart';

class PositionUseCase {
  MenuLateralPosition menuLateralPosition = MenuLateralPosition();

  PositionalLateralMenu positionalLateralMenu({required DragUpdateDetails details}) {
    return (
      verticalPos: 0.0,
      horizontalPos: details.globalPosition.dx,
      width: details.globalPosition.dx < 100
          ? 100
          : details.globalPosition.dx > 300
              ? 300
              : details.globalPosition.dx,
      height: menuLateralPosition.onResize(details: details).height
    );
  }
}

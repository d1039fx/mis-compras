import 'package:flutter/material.dart';
import 'package:to_buy_app/principal/bloc/responsive_bloc.dart';
import 'package:to_buy_app/principal/presentation/widgets/menu/lateral_menu.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InitialPage extends StatelessWidget {
  const InitialPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mercado'),
      ),
      body: LayoutBuilder(builder: (context, size) {
        return BlocBuilder<ResponsiveBloc, ResponsiveState>(
          builder: (context, state) {
            if (state is ResponsiveLateralMenu) {
              return Stack(
                fit: StackFit.expand,
                children: [
                  Positioned(
                      right: 0.0,
                      top: 0.0,
                      width: size.maxWidth >= size.maxHeight ? size.maxWidth - state.positionalLateralMenu.width : size.maxWidth,
                      height: size.maxWidth >= size.maxHeight ? size.maxHeight : size.maxHeight - 100,
                      child: const Placeholder()),
                  Positioned(
                    width: size.maxWidth >= size.maxHeight ? state.positionalLateralMenu.width : size.maxWidth,
                    height: size.maxWidth >= size.maxHeight ? size.maxHeight : 100,
                    bottom: 0.0,
                    child: GestureDetector(onPanUpdate: (details) => context.read<ResponsiveBloc>().add(LateralMenuPosition(details: details)), child: LateralMenu()),
                  ),
                ],
              );
            }
            return Stack(
              fit: StackFit.expand,
              children: [
                Positioned(left: size.maxWidth >= size.maxHeight ? 100 : 0, width: size.maxWidth >= size.maxHeight ? size.maxWidth - 100 : size.maxWidth, height: size.maxWidth >= size.maxHeight ? size.maxHeight : size.maxHeight - 100, child: const Placeholder()),
                AnimatedPositioned(duration: const Duration(milliseconds: 500),
                  bottom: 0,
                  left: 0,
                  width: size.maxWidth <= size.maxHeight ? size.maxWidth : 100,
                  height: size.maxWidth >= size.maxHeight ? size.maxHeight : 100,
                  child: GestureDetector(onPanUpdate: (details) => context.read<ResponsiveBloc>().add(LateralMenuPosition(details: details)), child: LateralMenu()),
                ),
              ],
            );
          }
        );
      }),
    );
  }
}

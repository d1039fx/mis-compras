import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/data_manager/auth/auth_data_manager.dart';
import 'package:to_buy_app/login/dominio/use_case/auth/auth.dart';

class LateralMenu extends StatelessWidget {
  LateralMenu({super.key});

  final AuthUseCase authUseCase = AuthUseCase(authDataManager: AuthDataManager());

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          Expanded(flex: 2, child: Column()),
          Divider(),
          Expanded(flex: 3, child: Column()),
          Divider(),
          Expanded(
            flex: 2,
            child: Center(
                child: IconButton(
                    onPressed: () => authUseCase.logout(context: context),
                    icon: Icon(Icons.logout))),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/gestures.dart';
import 'package:to_buy_app/principal/domain/interfaces/positional_lateral_interface.dart';

class MenuLateralPosition extends PositionalLateralInterfaces {
  @override
  ({double x, double y}) onDrag({required DragUpdateDetails details}) =>
      (x: details.delta.dx, y: details.delta.dy);

  @override
  ({double height, double width}) onResize({required DragUpdateDetails details}) =>
      (height: details.delta.dy, width: details.delta.dx);
}

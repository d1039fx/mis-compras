import 'package:go_router/go_router.dart';
import 'package:to_buy_app/principal/presentation/initial_page.dart';

mixin PrincipalRouter{
  List<GoRoute> pathPrincipalList() => <GoRoute>[
    GoRoute(path: '/initial_page', builder: (context, state) => InitialPage())
  ];
}
part of 'responsive_bloc.dart';

@immutable
sealed class ResponsiveState {}

final class ResponsiveInitial extends ResponsiveState {}

final class ResponsiveLateralMenu extends ResponsiveState{
  final PositionalLateralMenu positionalLateralMenu;

  ResponsiveLateralMenu({required this.positionalLateralMenu});
}

import 'package:bloc/bloc.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:to_buy_app/principal/domain/entities/lateral_menu/positional_lateral_menu.dart';
import 'package:to_buy_app/principal/domain/use_cases/menu_lateral/position_use_case.dart';

part 'responsive_event.dart';
part 'responsive_state.dart';

class ResponsiveBloc extends Bloc<ResponsiveEvent, ResponsiveState> {
  PositionalLateralMenu positionalLateralMenu =
      (horizontalPos: 0.0, verticalPos: 0.0, height: 150, width: 100);
  PositionUseCase positionUseCase = PositionUseCase();

  ResponsiveBloc() : super(ResponsiveInitial()) {
    on<LateralMenuPosition>((event, emit) {
      positionalLateralMenu = positionUseCase.positionalLateralMenu(details: event.details);
      emit(ResponsiveLateralMenu(positionalLateralMenu: positionalLateralMenu));
    });
  }
}

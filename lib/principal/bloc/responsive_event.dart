part of 'responsive_bloc.dart';

@immutable
sealed class ResponsiveEvent {}

class LateralMenuPosition extends ResponsiveEvent{
  final DragUpdateDetails details;

  LateralMenuPosition({required this.details});
}

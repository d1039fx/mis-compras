import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:to_buy_app/login/data/models/user_auth_model.dart';
import 'package:to_buy_app/login/dominio/interfaces/auth/auth_interfaces.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthDataManager extends AuthInterface {
  @override
  void registerUser({required UserAuth userAuth}) {
    try {
      FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: userAuth.email, password: userAuth.password)
          .then((onValue) {
        GoRouter.of(userAuth.context).go('/initial_page');
      });
    } on FirebaseAuthException catch (error) {
      if (error.code == 'weak-password') {
        if (userAuth.context.mounted) {
          ScaffoldMessenger.of(userAuth.context)
              .showSnackBar(SnackBar(content: Text('The password provided is too weak.')));
        }
      }
      if (error.code == 'email-already-in-use') {
        if (userAuth.context.mounted) {
          ScaffoldMessenger.of(userAuth.context)
              .showSnackBar(SnackBar(content: Text('The account already exists for that email.')));
        }
      }
    } catch (error) {
      if (kDebugMode) {
        print(error);
      }
      throw Exception(error);
    }
  }

  @override
  validationLoginForm({required UserAuth userAuth}) {
    if (userAuth.formState) {
      try{
        FirebaseAuth.instance.signInWithEmailAndPassword(email: userAuth.email, password: userAuth.password).then((value){
          userAuth.context.go('/initial_page');
        });
      } on FirebaseAuthException catch (error){
        if (userAuth.context.mounted) {
          ScaffoldMessenger.of(userAuth.context)
              .showSnackBar(SnackBar(content: Text('${error.message}')));
        }
      } catch (error){
        if(kDebugMode){
          print(error);
        }
        throw Exception(error);
      }
    }
  }

  @override
  void logoutUser({required BuildContext context}) {
    try{
      FirebaseAuth.instance.signOut().then((onValue){
        context.go('/login');
      });
    } on FirebaseAuthException catch (error){
      if(kDebugMode){
        print(error.message);
      }
      throw Exception(error);
    } catch (error){
      if(kDebugMode){
        print(error);
      }
      throw Exception(error);
    }

  }

  @override
  void showSplashScreen({required BuildContext context}) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        if (context.mounted) {
          context.go('/login');
        }
      } else {
        if (context.mounted) {
          context.go('/initial_page');
        }
      }
    }).onError((error) {
      if (context.mounted) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('$error')));
      }
    });
  }
}

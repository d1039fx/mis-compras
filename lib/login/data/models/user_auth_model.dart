import 'package:flutter/cupertino.dart';

typedef UserAuth = ({String email, String password, bool formState, BuildContext context});

UserAuth userAuthFromMap(Map<String, dynamic> json) => (
      email: json['email'],
      password: json['password'],
      formState: json['formState'],
      context: json['context']
    );

Map<String, dynamic> userAuthToMap({required UserAuth userAuth}) => {
      'email': userAuth.email,
      'password': userAuth.password,
      'formState': userAuth.formState,
      'context': userAuth.context
    };

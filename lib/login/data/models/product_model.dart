typedef Product = ({String? id, String? name, double? cost, int? cant});

Product productFromMap(Map<String, dynamic> json) =>
    (id: json['id'], cant: json['cant'], cost: json['cost'], name: json['name']);

Map<String, dynamic> productToMap({required Product product}) => {
  'id': product.id,
  'name': product.name,
  'cant': product.cant,
  'cost': product.cost
};

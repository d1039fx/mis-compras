

import 'package:to_buy_app/login/dominio/entity/user/user.dart';

abstract class UserData{
  User readUser();
  bool removeUser({required String id});
  User createUser();
  User updateUser({required String id});
}
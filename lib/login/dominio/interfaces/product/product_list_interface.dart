
import 'package:to_buy_app/login/dominio/entity/product/product.dart';

abstract class ProductListData{
  List<Product> readProductList();
  List<Product> removeProductList();
  List<Product> updateProductList();
  List<Product> createProductList();
}
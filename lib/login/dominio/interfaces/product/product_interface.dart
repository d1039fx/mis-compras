
import 'package:to_buy_app/login/dominio/entity/product/product.dart';

abstract class ProductData{
  Product selectedProduct();
  bool removedProduct({required String productId});
  Product updateProduct({required String productId});
  Product createProduct();
}
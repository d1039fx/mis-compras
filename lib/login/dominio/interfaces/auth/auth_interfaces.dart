import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/models/user_auth_model.dart';

abstract class AuthInterface {
  void registerUser({required UserAuth userAuth});
  void validationLoginForm({required UserAuth userAuth});
  void logoutUser({required BuildContext context});
  void showSplashScreen({required BuildContext context});
}

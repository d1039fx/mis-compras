class Product{
  final String name;
  final int cant;
  final String description;
  final double cost;

  Product({required this.name, required this.cant, required this.description, required this.cost});
}


import 'package:to_buy_app/login/dominio/entity/product/product.dart';

class ProductListUseCase{

  List<Product> readProductList(){
    return <Product>[];
  }

  List<Product> updateProductList(){
    return <Product>[];
  }

  bool removeProductList(){
    return false;
  }

  List<Product> createProductList(){
    return <Product>[];
  }
}
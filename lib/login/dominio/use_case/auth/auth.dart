import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/data_manager/auth/auth_data_manager.dart';
import 'package:to_buy_app/login/data/models/user_auth_model.dart';

class AuthUseCase {
  final AuthDataManager authDataManager;

  AuthUseCase({required this.authDataManager});

  void validationRegister({required UserAuth userAuth}) =>
      authDataManager.registerUser(userAuth: userAuth);

  void validationLogin({required UserAuth userAuth}) =>
      authDataManager.validationLoginForm(userAuth: userAuth);

  void logout({required BuildContext context}) => authDataManager.logoutUser(context: context);

  void showSplashScreen({required BuildContext context}) =>
      authDataManager.showSplashScreen(context: context);
}

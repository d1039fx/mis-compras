

import 'package:go_router/go_router.dart';
import 'package:to_buy_app/login/presentation/login_page/page.dart';
import 'package:to_buy_app/login/presentation/splash_screen_page/splash_screen.dart';
import 'package:to_buy_app/login/presentation/subscribe_widget/subcribe/subscribe_page.dart';

mixin LoginRouter {
  List<GoRoute> pathLoginList() => <GoRoute>[
    GoRoute(path: '/login', builder: (context, state) => const LoginPage(),),
    GoRoute(path: '/subscribe', builder: (context, state) => SubscribePage(),),
    GoRoute(path: '/splash', builder: (context, state) => SplashScreen(),),
  ];


}

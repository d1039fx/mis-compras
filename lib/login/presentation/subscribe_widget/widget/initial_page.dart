import 'package:flutter/material.dart';

class InitialPage extends StatelessWidget {
  final String titleApp;
  const InitialPage({super.key, required this.titleApp});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(titleApp),),
    );
  }
}



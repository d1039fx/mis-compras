import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/data_manager/auth/auth_data_manager.dart';
import 'package:to_buy_app/login/dominio/use_case/auth/auth.dart';
import 'package:to_buy_app/login/presentation/general_widgets/inputs/general_input_form.dart';

class SubscribePage extends StatelessWidget {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final AuthUseCase authUseCase = AuthUseCase(authDataManager: AuthDataManager());

  SubscribePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registro de usuario'),
      ),
      body: Form(
          key: _form,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              spacing: 20,
              children: [
                GeneralInputForm(
                    input: email, title: 'Ingresar Email', emptyInput: 'Email no ingresado'),
                GeneralInputForm(
                    input: password, title: 'Ingresar Password', emptyInput: 'Password no ingresado'),
                GeneralInputForm(
                    input: confirmPassword,
                    title: 'Ingresar Password de nuevo',
                    emptyInput: 'Password no ingresado'),
                ElevatedButton(
                    onPressed: () => authUseCase.validationRegister(userAuth: (
                          email: email.text,
                          password: password.text,
                          formState: _form.currentState?.validate() ?? false,
                          context: context
                        )),
                    child: Text('Registrarse'))
              ],
            ),
          )),
    );
  }
}

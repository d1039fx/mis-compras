import 'package:flutter/material.dart';

class GeneralButton extends StatelessWidget {
  final Function function;
  const GeneralButton({super.key, required this.function});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: function(),
        child: Text('Login'));
  }
}

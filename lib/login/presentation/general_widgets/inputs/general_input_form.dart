import 'package:flutter/material.dart';

class GeneralInputForm extends StatelessWidget {
  final TextEditingController input;
  final String title;
  final String emptyInput;

  const GeneralInputForm(
      {super.key, required this.input, required this.title, required this.emptyInput});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: input,
      decoration: InputDecoration(hintText: title, border: OutlineInputBorder()),
      validator: (value) {
        if ((value ?? '').isEmpty) {
          return emptyInput;
        }
        return null;
      },
    );
  }
}

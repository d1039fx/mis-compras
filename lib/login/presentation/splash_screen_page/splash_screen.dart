import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/data_manager/auth/auth_data_manager.dart';
import 'package:to_buy_app/login/dominio/use_case/auth/auth.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthUseCase authUseCase = AuthUseCase(authDataManager: AuthDataManager());

  @override
  void initState() {
    Future.delayed(Duration(seconds: 1)).then((value) {
      if (mounted) {
        authUseCase.showSplashScreen(context: context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: const Placeholder());
  }
}

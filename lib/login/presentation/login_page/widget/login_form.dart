import 'package:flutter/material.dart';
import 'package:to_buy_app/login/data/data_manager/auth/auth_data_manager.dart';
import 'package:to_buy_app/login/dominio/use_case/auth/auth.dart';
import 'package:to_buy_app/login/presentation/general_widgets/inputs/general_input_form.dart';
import 'package:to_buy_app/login/presentation/subscribe_widget/subcribe/subscribe_page.dart';

class LoginForm extends StatelessWidget {
  LoginForm({super.key});

  final _formKey = GlobalKey<FormState>();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();
  final AuthUseCase authUseCase = AuthUseCase(authDataManager: AuthDataManager());

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child: Column(
          spacing: 20,
          children: [
            GeneralInputForm(
                input: email, title: 'Ingresar email', emptyInput: 'Email no ingresado'),
            GeneralInputForm(
                input: password, title: 'Password', emptyInput: 'Password no ingresado'),
            ElevatedButton(
                onPressed: () => authUseCase.validationLogin(userAuth: (
                      context: context,
                      formState: _formKey.currentState?.validate() ?? false,
                      password: password.text,
                      email: email.text
                    )),
                child: Text('Login')),
            TextButton(
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => SubscribePage())),
                child: Text('Registrarse'))
          ],
        ),
      ),
    );
  }
}
